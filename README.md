# megarepo

Mega Repo for All Things Tech
The internet today provides a vast amount of knowledge in Tech and almost anything and everything human beings know.
The main purpose of this repo is to curate, refine and aggregate all the information online.

Project Structure 

Level 0
- Industry

Level 1
- Fields of study

Level 2
- Sub Fields of study

Level 3
- Specialization


Sample:
- Technology
    - Software 
        - Programming Languages 
            - Python
